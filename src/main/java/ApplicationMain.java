import com.sevtech.news.Java10News;
import com.sevtech.news.Java11News;
import com.sevtech.news.Java9News;

public class ApplicationMain {

    public static void main(String[] args) {

        //NOVEDADES JAVA 9

        Java9News.collectionsMethods();
        Java9News.streamsMethods();
        Java9News.optionalMethods();
        Java9News.futureMethods();

        //NOVEDADES JAVA 10

        Java10News.optionalMethods();
        Java10News.collectionsMethods();

        //NOVEDADES JAVA 11

        Java11News.stringMethods();
        Java11News.inferenceTypes();

    }
}
