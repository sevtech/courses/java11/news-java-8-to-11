public interface PrivateMethodInterface {

    default int sumaPositivos() {
        return suma(1, 2);
    }

    default int sumaNegativos() {
        return suma(-1, -2);
    }

    @Deprecated(forRemoval = true, since = "1.2")
    private int suma(int a, int b) {
        return a + b;
    }
}
