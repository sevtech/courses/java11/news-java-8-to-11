package com.sevtech.news;

import lombok.extern.java.Log;

import java.util.Map;

import static java.util.stream.Collectors.toList;

@Log
public final class Java11News {

    public static void stringMethods() {
        log.info("Ejemplo de repetición: " + "a,".repeat(3));
        log.info("Ejemplo de stripLeading: " + "   a,   ".stripLeading() + ",");
        log.info("Ejemplo de stripTrailing: " + "   a,   ".stripTrailing() + ",");
        log.info("Ejemplo de isBlank: " + "   a,".isBlank());
        log.info("Ejemplo de lines:" + "Esto\nes\nun\nejemplo".lines().collect(toList()));
    }

    public static void inferenceTypes() {
        log.info("\nEjemplo de inferencia de tipos y anotaciones en lambdas");
        Map<Integer, Integer> mapConInferencia = Map.of(1, 2, 3, 4, 5, 6);
        mapConInferencia.forEach((x, y) -> System.out.print(x + y + ","));
        System.out.println();
        mapConInferencia.forEach((Integer x, Integer y) -> System.out.print(x + y + ","));
        System.out.println();
        mapConInferencia.forEach((var x, var y) -> System.out.print(x + y + ","));
        System.out.println();
        mapConInferencia.forEach((@Deprecated var x, var y) -> System.out.print(x + y + ","));
    }
}
