package com.sevtech.news;

import lombok.extern.java.Log;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Log
public final class Java9News {

    public static void collectionsMethods() {
        log.info("Mostrando los nuevos métodos para crear colecciones inmutables");
        List<Integer> list = List.of(1, 2, 3);
        Set<String> set = Set.of("Java", "Kotlin", "Groovy");
        Stream<String> stream = Stream.of("Java", "Kotlin", "Groovy");
        Map<String, String> map = Map.of("Lenguaje", "Java", "Framework", "Spring");

        log.info("Lista inmutable: " + list);
        log.info("Set inmutable: " + set);
        log.info("Stream inmutable: " + stream.collect(toList()));
        log.info("Map inmutable: " + map);

        try {
            //Añadimos un elemento a una colección inmutable
            set.add("Python");
        } catch (UnsupportedOperationException exception) {
            log.severe("Error al añadir un elemento a una colección inmutable");
        }
    }

    public static void streamsMethods() {
        log.info("Mostrando los nuevos métodos de los streams");
        List takeWhileResult = Stream.of(1, 2, 3, 4, 5).takeWhile(value -> value < 3).collect(toList());
        log.info("Ejemplo takeWhile():" + takeWhileResult);

        List dropWhileResult = Stream.of(1, 2, 3, 4, 5).dropWhile(value -> value < 3).collect(toList());
        log.info("Ejemplo dropWhile(): " + dropWhileResult);

        List iterateResult = Stream.iterate(1L, n -> n + 1).limit(10).collect(toList());
        log.info("Ejemplo iterate(): " + iterateResult);

        String example = "example";
        List ofNullableResult = Stream.ofNullable(example).collect(toList());
        log.info("Ejemplo ofNullable() con valor: " + ofNullableResult);
        String nullExample = null;
        List ofNullableNullResult = Stream.ofNullable(nullExample).collect(toList());
        log.info("Ejemplo ofNullable() sin valor: " + ofNullableNullResult);
    }

    public static void optionalMethods() {
        log.info("Mostrando los nuevos métodos de Optional");
        Optional<String> optional1 = Optional.of("Texto 1");
        Optional<String> optional2 = Optional.empty();
        Optional<String> optional3 = Optional.of("Texto 3");

        List<String> textos = Stream.of(optional1, optional2, optional3)
                .flatMap(Optional::stream).collect(toList());

        log.info("Ejemplo método Stream en Optional: " + textos);

        log.info("\nEjemplo ifPresentOrElse");
        Optional<String> optionalConUnTexto = Optional.of("Un texto");
        Optional<String> optionalVacio = Optional.empty();

        optionalConUnTexto.ifPresentOrElse(
                texto -> {
                    log.info("El optional con texto debería pasar por el present.");
                },
                () -> {
                    throw new RuntimeException("El optional con texto nunca debería pasar por el else");
                }
        );

        optionalVacio.ifPresentOrElse(
                texto -> {
                    throw new RuntimeException("El optional vacío nunca debería pasar por el present");
                },
                () -> log.info("El optional vacío debería pasar por el else")
        );

        log.info("\nEjemplo método Or()");
        Optional orOptionalConUnTexto = optionalConUnTexto.or(() -> Optional.of("Optional vacío"));
        Optional orOptionalVacio = optionalVacio.or(() -> Optional.of("Optional vacío"));
        log.info("Con un Optional con texto" + orOptionalConUnTexto.get());
        log.info("Con un Optional sin texto" + orOptionalVacio.get());
    }

    public static void futureMethods() {
        log.info("\nFUTUROS");
        CompletableFuture future = CompletableFuture.runAsync(() -> log.info("Creando un Futuro"));
        future.orTimeout(10, TimeUnit.MILLISECONDS);
        future.completeOnTimeout("Futuro terminado por timeout", 1, TimeUnit.MICROSECONDS);
    }
}
