package com.sevtech.news;

import lombok.extern.java.Log;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toUnmodifiableList;
import static java.util.stream.Collectors.toUnmodifiableMap;

@Log
public final class Java10News {

    public static void optionalMethods() {
        log.info("\nMétodo orElseThrow()");
        try {
            Optional.empty().orElseThrow(() -> new Exception("El optional era nulo"));
        } catch (Exception e) {
            log.info(e.getMessage());
        }

    }

    public static void collectionsMethods() {
        log.info("\nCreación de colleciones inmutables");

        List toUnmodifiableList = Stream.of("a", "b", "c").collect(toUnmodifiableList());
        Set toUnmodifiableSet = Stream.of("g", "h", "i").collect(Collectors.toUnmodifiableSet());
        Map<Integer, Integer> toUnmodifiableMap = Stream.of(1, 2, 3).collect(toUnmodifiableMap(
                num -> num,
                num -> num * 3));

        log.info("Lista inmutable: " + toUnmodifiableList);
        log.info("Set inmutable: " + toUnmodifiableSet);
        log.info("Map inmutable: " + toUnmodifiableMap);
    }
}
